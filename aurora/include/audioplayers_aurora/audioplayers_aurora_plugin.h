#ifndef FLUTTER_PLUGIN_AUDIOPLAYERS_AURORA_PLUGIN_H
#define FLUTTER_PLUGIN_AUDIOPLAYERS_AURORA_PLUGIN_H

#include <audioplayers_aurora/globals.h>
#include <flutter/plugin-interface.h>

#include <../audio_player.h>

class PLUGIN_EXPORT AudioplayersAuroraPlugin final : public PluginInterface {
 public:
  void RegisterWithRegistrar(PluginRegistrar& registrar) override;
 private:
  std::map<std::string, std::unique_ptr<AudioPlayer>> audioPlayers;

  void CreatePlayer(const std::string& playerId);
  AudioPlayer* GetPlayer(const std::string& playerId);
  void OnGlobalLog(const gchar* message);
  void HandleGlobalMethodCall(const MethodCall& method_call);
  void HandleMethodCall(const MethodCall& method_call);
  void Dispose();

  void onMethodCall(const MethodCall& call);
  void onMethodCallGlobal(const MethodCall& call);
  void unimplemented(const MethodCall& call);
};

#endif /* FLUTTER_PLUGIN_AUDIOPLAYERS_AURORA_PLUGIN_H */
